# Steps to infer tree with IQ-TREE using data from Marin et al. 2013 and WAM
### Version 1.0
### Consult: Ian Brennan

1. Download genetic sequences 
      --Software: Geneious
        - Got alignment file from Ian Brennan. New sequences from WAM mostly *A. ligatus, A. grypus, A. margaretae, A. pinguis, A. splendidus* 

2. Align sequence (seq_01_alignment.R)
      --Software: GENEIOUS R9
        - Aligned using MUSCLE

3. Check alignment by eye. If not good, manually change

4. Trim sequence and export with terminal gaps with 'N' 
      --Software: GENEIOUS R9

5. IQ-TREE 2 to infer bootstrap tree 
      --Software: IQTREE 
      powershell command: iqtree -s *.fasta -bb 1000 -nt AUTO
        1000 bootstrap. -nt = nucleotide substitution auto model finding.
