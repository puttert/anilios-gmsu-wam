# filename: 00_downloadGenBank.R
# Putter Tiatragul 
# October 2020. Code adapted from jcsantos. This is to download 

# Packages ----------------------------------------------------------------
library(ape)
library(readr)
library(dplyr)
library(seqinr)

# Download sequences from Genbank -----------------------------------------
# Marin et al. 2013 sequenced over 700 gene samples (mt *Cyt b* and nc *PRLR* ) of many Australian blind snake species.
# I downloaded their data (including the GenBank accession number) from the Biological Journal of the Linnean Society publication *doi.org/10.1111/bij.12132*.
# Using the accession number listed, I can download the sequences in R using the `ape::read.Genbank` function.
# We cna then create a vector with more useful names such that we can rename the *header* of each sequence once it is read in as a `fasta` file.


MarinEtAl2013 <- read_csv("data/MarinEtAl2013_BJLSSuppData_outgroup.csv",
                          col_types = cols(SH = col_character())) %>% 
  mutate(sp.group = paste(Marin_species, SH, sep = ""))

# Cytochrome B GenBank number vector
cytb <- MarinEtAl2013$Cytb 

# PRLR in a different column has less samples. 
# prlr <- MarinEtAl2013$PRLR
# prlr <- prlr[!is.na(prlr)] # remove NAs

# Download GenBank data (this may take a long time depending on how many sequences)
ani.seq.cytb <- ape::read.GenBank(cytb)
# ani.seq.prlr <- ape::read.GenBank(prlr)

# print attribute of this object (ani.seq.cytb): names, class, description, and species.
# attributes(ani.seq.cytb)
# attributes(ani.seq.prlr)

# print just the description of each sequence to check if it is correct
attr(ani.seq.cytb, "description")
# attr(ani.seq.prlr, "description")

# Write it as a single fasta file
# write as a new cleaned fasta
write.dna(ani.seq.cytb, file = "data/genetic_data/MarinEtAl13_cytb.fasta", format = "fasta",
               append = FALSE, nbcol = 6, colsep = " ", colw = 10)

# write.dna(ani.seq.prlr, file = "data/molecular.data/MarinEtAl2013/MarinEtAl13_prlr.fasta", format = "fasta",
# append = FALSE, nbcol = 6, colsep = " ", colw = 10)


# Rename ------------------------------------------------------------------
# Name based on species and SH
name.cytb <- MarinEtAl2013$sp.group

#for prlr have to first filter out ones without sequence
tmp <- MarinEtAl2013 %>% 
  filter(!is.na(PRLR)) %>% 
  select(sp.group) 

name.prlr <- tmp[[1]]


# Rewrite fasta file with more info ---------------------------------------
library(seqinr)

ani.cytb.seqinr_format <- read.fasta(file = "data/genetic_data/MarinEtAl13_cytb.fasta", 
                                    whole.header = TRUE)

# ani.prlr.seqinr_format <- read.fasta(file = "data/molecular.data/MarinEtAl2013/MarinEtAl13_prlr.fasta", 
#                                     whole.header = TRUE)

# Make a vector of names + accession number
ani.cytb.GenBank.IDs <- paste(name.cytb, names(ani.cytb.seqinr_format), sep = "_cytb_")
# ani.prlr.GenBank.IDs <- paste(name.prlr, names(ani.prlr.seqinr_format), sep = "_prlr_")


# create a vector with more user-friendly name for each sequence (check)
head(ani.cytb.GenBank.IDs)


# Rewrite it with better header names by specifying names =
write.fasta(sequences = ani.cytb.seqinr_format, 
            names = ani.cytb.GenBank.IDs,
            file.out = "data/genetic_data/MarinEtAl13_cytb_clean.fasta")