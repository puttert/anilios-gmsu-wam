require(phytools)
require(stringr)
require(ape)


# The first function will trim a TREE according to generic names provided:
# phy: either a single tree or a multiPhylo object
# groups: a vector of groups names e.g. c("Petrogale", "Macropus")
# method: either "keep" the names you've designated or "drop" them

extract.by.group <- function(phy, groups, method = c("keep", "drop")){
  all.names <- NULL
  
  if(class(phy) == "phylo"){phy.names <- phy$tip.label}
  if(class(phy) == "multiPhylo"){phy.names <- phy[[1]]$tip.label}
  
  for (j in 1:length(groups)){
    curr.names <- phy.names[which(!is.na(str_extract(phy.names, groups[j])))]
    all.names <- append(all.names, curr.names)
  }
  
  if(method == "keep") {
    print(paste("keeping:", all.names))
    print(paste("dropping:", setdiff(phy.names, all.names)))
    if(class(phy) == "phylo"){new.phy <- drop.tip(phy, setdiff(phy.names, all.names))}
    if(class(phy) == "multiPhylo"){new.phy <- lapply(phy, drop.tip, tip=setdiff(phy.names, all.names));
    class(new.phy) <- "multiPhylo"}
  }

  if(method == "drop") {
    print(paste("dropping:", all.names))
    print(paste("keeping:", setdiff(phy.names, all.names)))
    if(class(phy) == "phylo"){new.phy <- drop.tip(phy, all.names)}
    if(class(phy) == "multiPhylo"){new.phy <- lapply(phy, drop.tip, tip=all.names);
    class(new.phy) <- "multiPhylo"}
  }
  
  return(new.phy)
}

# The second function will trim an ALIGNMENT according to generic names provided:
# align: an alignment in fasta, phylip, or nexus format, stored as a list or DNAbin using read.FASTA(), read.dna(), or read.nexus.data()
# align.type: the format for the alignment (fasta, nexus, phylip)
# groups: a vector of groups names e.g. c("Petrogale", "Macropus")
# method: either "keep" the names you've designated or "drop" them

extract.alignment.group <- function(align, align.type = c("fasta", "nexus", "phylip"),
                                    groups, method = c("keep", "drop")){
  all.names <- NULL
  
  if(length(names(align)) == 0){
    align.names <- rownames(align)
    } else {align.names <- names(align)}
  
  for (j in 1:length(groups)){
    curr.names <- align.names[which(!is.na(str_extract(align.names, groups[j])))]
    all.names <- append(all.names, curr.names)
  }
  
  if(method == "keep") {
    print(paste("keeping:", all.names))
    print(paste("dropping:", setdiff(align.names, all.names)))
    if(align.type == "fasta"){new.align <- align[which(names(align) %in% all.names)]}
    if(align.type == "phylip"){new.align <- align[which(rownames(align) %in% all.names),]}
    if(align.type == "nexus"){new.align <- align[which(names(align) %in% all.names)]}
  }
  
  if(method == "drop") {
    print(paste("dropping:", all.names))
    print(paste("keeping:", setdiff(align.names, all.names)))
    if(align.type == "fasta"){new.align <- align[which(!names(align) %in% all.names)]}
    if(align.type == "phylip"){new.align <- align[which(!rownames(align) %in% all.names),]}
    if(align.type == "nexus"){new.align <- align[which(!names(align) %in% all.names)]}
  }
  
  return(new.align)
}





